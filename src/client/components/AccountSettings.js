import React, { Component } from "react";

import { Button, TextField } from "@material-ui/core";
import { Link } from "react-router-dom";
import CssBaseline from "@material-ui/core/CssBaseline";

import "./AccountSettings.scss";

class AccountSettings extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            email: ""
        };
    }

    handleChange(event) {
        this.setState({
            email: event.target.value
        });
    }

    render() {
        return(
            <div id="settings">
                <CssBaseline />
                <TextField
                    label="Registration Email"
                    value={this.state.email}
                    onChange={this.handleChange}
                    margin="normal"
                />
                <Button component={ Link } to={ "/api/account/delete/" + this.state.email } target="_self">Delete Account</Button>
            </div>
        );
    }
}

export default AccountSettings;