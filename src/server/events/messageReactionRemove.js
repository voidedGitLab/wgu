import Suggestion from "../database/models/Suggestion";

export default (client, wss, reaction, user) => {
    var message = reaction.message;
    var emoji = reaction.emoji;
    var author = message.author;
    var guild = message.guild;

    var member = guild.members.find("id", user.id);

    var reacted = reaction.me;

    if(user === client.user) return;

    if(reacted) {
        var f = -1;
        switch(emoji.name) {
        case "⬆":
            Suggestion.findOneAndUpdate({ "message.id": message.id }, { $inc: {"stats.upvotes": f} }, (err, doc) => {
                Suggestion.findOne({ "message.id": message.id }, (err, res) => {
                    wss.broadcast({ action: "update", replace: res });
                });
            });
            break;
        case "↔":
            Suggestion.findOneAndUpdate({ "message.id": message.id }, { $inc: {"stats.middlevotes": f} }, (err, doc) => {
                Suggestion.findOne({ "message.id": message.id }, (err, res) => {
                    wss.broadcast({ action: "update", replace: res });
                });
            });
            break;
        case "⬇":
            Suggestion.findOneAndUpdate({ "message.id": message.id }, { $inc: {"stats.downvotes": f} }, (err, doc) => {
                Suggestion.findOne({ "message.id": message.id }, (err, res) => {
                    wss.broadcast({ action: "update", replace: res });
                });
            });
            break;
        }
    }
};